//
//  Person+CoreDataProperties.swift
//  CoreData_Example_02
//
//  Created by Micky on 17.11.2020.
//
//

import Foundation
import CoreData


extension Person {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Person> {
        return NSFetchRequest<Person>(entityName: "Person")
    }

    @NSManaged public var name: String?
    @NSManaged public var age: Int64
    @NSManaged public var gender: String?

}

extension Person : Identifiable {

}
